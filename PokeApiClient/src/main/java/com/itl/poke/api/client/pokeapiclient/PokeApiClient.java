/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itl.poke.api.client.pokeapiclient;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import com.itl.poke.api.pokemon.Pokemon;

/**
 *
 * @author maquina
 */
public class PokeApiClient {

//    public static void main(String[] args) {
//        try {
//            String json = webServiceCliente();
//            System.out.println("listaVal=" + json);
//            System.out.println("-->"+parseList(json).size());
////            URL url = new URL("https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0");
////            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
////            conn.setRequestMethod("GET");
////            conn.setRequestProperty("Accept", "application/json");
////
////            if (conn.getResponseCode() != 200) {
////                throw new RuntimeException("Failed : HTTP error code : "
////                        + conn.getResponseCode());
////            }
////
////            BufferedReader br = new BufferedReader(new InputStreamReader(
////                    (conn.getInputStream())));
////
////            String output, json = "";
////            System.out.println("Output from Server .... \n");
////            while ((output = br.readLine()) != null) {
////                //System.out.println(output);
////                json += output;
////            }
////            System.err.println(json);
////            conn.disconnect();
////            JsonObject jobj = new Gson().fromJson(json, JsonObject.class);
////            System.err.println(jobj.getAsJsonArray("results"));
////            //String result = jobj.get("results").getAsString();
////            //System.out.println(result);
////            JsonElement jsonElement = jobj.getAsJsonArray("results");
////            Type listType = new TypeToken<List<Pokemon>>() {
////            }.getType();
////            List<String> yourList = new Gson().fromJson(jsonElement, listType);
////
////            
////            System.out.println("longitud del arreglo "+ yourList.size());
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//    }

    public static List<Pokemon> parseList(String info) {

        JsonObject jobj = new Gson().fromJson(info, JsonObject.class);
        JsonElement jsonElement = jobj.getAsJsonArray("results");
        Type listType = new TypeToken<List<Pokemon>>() {
        }.getType();
        List<Pokemon> pokeList = new Gson().fromJson(jsonElement, listType);
        System.out.println("longitud del arreglo " + pokeList.size());
        return pokeList;
    }

    public static String webServiceCliente() {
        String json = "";
        try {
            URL url = new URL("https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                json += output;
            }
            conn.disconnect();
        } catch (Exception e) {
        }
        return json;
    }
}
